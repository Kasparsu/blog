<?php

namespace App\Http\Controllers;

use App\Commands;
use Illuminate\Http\Request;

class CMDController extends Controller
{
    public function index() {
		return view('template');
    }
	public function command(Request $request, Commands $commands) {
    	$command = $request->input('command');
		if(method_exists( $commands , $command)){
			return $commands->$command();
		} else {
			return $commands->unknownCommand();
		}
	}
}
