<?php


namespace App;


class Commands {
	public function help() {
		$helptext= "BLOG bash, version 0.0.1-alpha (browser)
These shell commands are defined internally.  Type `help' to see this list.
Type `help name' to find out more about the function `name'.";
		$helptext = str_replace("\n", '<br>', $helptext);
		return $helptext;
	}
	public function unknownCommand() {
		return 'unknown command';
	}
}